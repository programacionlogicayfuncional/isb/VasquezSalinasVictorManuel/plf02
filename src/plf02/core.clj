(ns plf02.core)

(ns plf02.core)

(defn función-asociativa-1
  [a]
  (associative? a))

(defn función-asociativaname-2
  [a]
  (associative? (associative? a)))

(defn función-asociativaconcat-3
  [a b]
  (associative? (concat a b)))


(función-asociativa-1 [1 2 3])
(función-asociativaname-2 "manuel")
(función-asociativaconcat-3 [1 2 3] [3 4 5])

;;boolean?
(defn función-boolean-1
  [a]
  (boolean? a))

(defn función-boolean-2
  [a b]
  (boolean? (> a b)))

(defn función-boolean-3
  [a]
  (boolean? (char? a)))

(función-boolean-1 (función-asociativa-1 [1 2 3]))
(función-boolean-2 3 4)
(función-boolean-3 'a')

;char?
(defn función-char-1
  [a]
  (char? (get a 0)))

(defn función-char-2
  [a]
  (char? (get (conj [] (filter char? a)) 0)))

(defn función-char-3
  [a]
  (char? (peek (conj () (filter char? a)))))


(función-char-1 [\a 1 2 3])
(función-char-2 [\d "manuel" 6 6 6])
(función-char-3 [\d "manuel" 6 6 6])

;cool?
(defn función-coll-1
  [a]
  (coll? (filter pos? a)))

(defn función-coll-2
  [a b]
  (coll? (concat  (conj [] (get a 0) conj [] (get b 2)))))

(defn función-coll-3
  [a]
  (coll? (map pos? a)))


(función-coll-1 [1 2 -33 4])
(función-coll-2 [1 2 3 4] ["Victor" "Manuel" "Salinas"])
(función-coll-3 [-6 -3 -4 -2])

;decimal?

(defn función-decimal-1 
  [a b]
  (decimal? (* a b)))

(defn función-decimal-2
  [a b]
  (decimal? (get a b)))

(defn función-decimal-3
  [a]
  (decimal? (filter decimal? a)))

(función-decimal-1 1M 1000000)
(función-decimal-2 [1 2 "manuel" 1M 1N] 3)
(función-decimal-3 [1 "manuel" \a])

;double 
(defn función-double-1
  [a b]
  (double? (/ a b)))

(defn función-double-2
  [a]
  (double? (first (map (fn [a] (/ 7 a)) a))))

(función-double-1 2 3.4)
(función-double-2 [3.4 6 7 2 4 4.5])

(defn función-double-3
  [a]
  (double? (Double. a)))

(función-double-3 "3.1416")